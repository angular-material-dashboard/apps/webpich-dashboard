# Webpich Dashboard

[![pipeline status](https://gitlab.com/angular-material-dashboard/apps/webpich-dashboard/badges/master/pipeline.svg)](https://gitlab.com/angular-material-dashboard/apps/webpich-dashboard/commits/master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/476107f7d1f940b292c3c1b2626ba4b3)](https://www.codacy.com/app/amd-apps/webpich-dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=angular-material-dashboard/apps/webpich-dashboard&amp;utm_campaign=Badge_Grade)

Main dashboard of sites on Webpich.